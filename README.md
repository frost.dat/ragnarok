# Сервер Ragnarok 
![](https://img.shields.io/gitlab/pipeline/frost.dat/Ragnarok)
## Introduction
Данный проект представляет собой реализацию [rAthena](https://rathena.org/) (это проект с открытым исходным кодом, эмулирующий Ragnarok Online
 Server) в окружении docker. 

## Deploy
### Окружение docker-compose
Для более быстрой развертки рекомендуется использовать <code>docker-compose.yml</code>.

### Запуск
Запуск docker-compose производится следующей командой:
```
docker-compose up -d
```
