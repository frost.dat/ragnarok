FROM ubuntu:20.04

USER root
WORKDIR /usr/bin/rathena/
COPY rathena /usr/bin/rathena/
COPY start.sh /

# Install required packages
RUN apt-get update \
    && apt-get install -y make libmysqlclient-dev zlib1g-dev libpcre3-dev build-essential

# Install mysql-server
RUN apt-get install -y mysql-server

# Compile server
RUN ./configure --enable-packetver=20180418 \
    && make server

# Create and configure database
RUN service mysql start \
    && mysql -u root -e "FLUSH PRIVILEGES;" \
    && mysql -u root -e "ALTER USER 'root'@'localhost' IDENTIFIED BY 'root_password';" \
    && mysql -u root --password=root_password -e "CREATE USER 'ragnarok'@'localhost' IDENTIFIED BY 'changeme';" \
    && mysql -u root --password=root_password -e "ALTER USER 'ragnarok'@'localhost' IDENTIFIED BY 'ragnarok';" \
    && mysql -u root --password=root_password -e "CREATE DATABASE ragnarok;" \
    && mysql -u root --password=root_password -e "GRANT ALL ON ragnarok.* TO 'ragnarok'@'localhost';" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/main.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/logs.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/item_db.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/item_db_re.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/item_db2.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/item_db2_re.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/item_cash_db.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/item_cash_db2.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/mob_db.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/mob_db2.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/mob_db2_re.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/mob_db_re.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/mob_skill_db.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/mob_skill_db2.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/mob_skill_db_re.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "USE ragnarok; source /usr/bin/rathena/sql-files/mob_skill_db2_re.sql;" \
    && mysql -u ragnarok --password=ragnarok -e "INSERT INTO ragnarok.login (userid, user_pass, group_id) VALUES ('testing', 'testing', '99')" \
    && mysql -u ragnarok --password=ragnarok -e "select * from ragnarok.login;" \
    && chmod a+x /*.sh

EXPOSE 3306 5121 6121 6900

ENTRYPOINT ["/start.sh"]
